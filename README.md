# nezumis.xyz

A personal website.

Currently bare-bones. Available on https://nezumis.xyz or https://鼠.xyz.

Current plans are to host a blog, a portfolio, a URL shortener, and potentially private image hosting. May be the site of a future social network project.

Developed by Lucia.

## Usage

This software is not intended for public use, but we fully permit any usage or editing of the software in accordance with the software's license.

To run, you will need to have Python. Only Python 3.9 has been tested for this program. First, install all dependencies from the requirements.txt file. Set the environment variable `FLASK_APP` to `nezumis`. Finally, run the `flask run` command. If this doesn't work, try not to ask us for 'tech support' about it; we learned how that goes the hard way.

This project uses the GNU General Public License v3. You may read the full license in the LICENSE.md file.