from flask import Flask
import os

def create_app(config={"ENV": "development", "SECRET_KEY": "dev"}):
	app = Flask(__name__)
	app.config.from_mapping(config)

	from . import base
	app.register_blueprint(base.bp)

	return app