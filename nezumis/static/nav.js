function navResponsive() {
  var navElement = document.getElementById("nav");
  if (navElement.className === "nav") {
    navElement.className += " responsive";
  } else {
    navElement.className = "nav";
  }
}